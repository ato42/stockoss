import React from 'react';

import './History.css';


function HistoryEntry({ search, time, onClick }) {
  const date = new Date(time);
  return (
    <div className="HistoryItem" onClick={onClick}>
      <div>{search}</div>
      <div>{date.toLocaleDateString()} {date.toLocaleTimeString()}</div>
    </div>
  );
}


function History({ history, onHistoryEntryClick }) {
  return (
    <div className="History">
      {history.map(item => (
        <HistoryEntry
          key={item.time}
          onClick={() => onHistoryEntryClick(item.search)}
          {...item}
        />
      ))}
    </div>
  );
}


export default History;