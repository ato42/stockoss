import React from 'react';

import './Map.css';

const rows = ['A', 'B', 'C', 'D', 'E'];
const cols = ['0', '1', '2', '3', '4', '5'];


function Cell({ row, col, selected }) {
  return (
    <div className={`Cell ${selected ? 'selected' : ''}`}>
      {row}{col}
    </div>
  )
}

function Grid({ cellSelected }) {
  return (
    <div className="Grid">
      {cols.map(col => (
        rows.map(row => (
          <Cell
            key={`${row}${col}`}
            row={row}
            col={col}
            selected={`${row.toLowerCase()}${col}` === cellSelected}
          />
        ))
      ))}
    </div>
  )
}


function Map({ position }) {
  return (
    <div className="Map">
      <Grid cellSelected={position} />
    </div>
  );
}


export default Map;