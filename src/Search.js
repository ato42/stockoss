import React, { useState } from 'react';

import './Search.css';


function SearchComponent({ onSearch, resetError, error, }) {
  const [search, setSearch] = useState('');

  const handleInputChange = (e) => {
    resetError();
    setSearch(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(search);
  };

  return (
    <form onSubmit={handleSubmit} className="SearchForm">
      <input
        className={`SearchInput ${error ? "error" : ""}`}
        type="text"
        value={search}
        onChange={handleInputChange}
        placeholder="Identifier"
      />
      <input
        className="SearchButton"
        type="submit"
        value="Search"
      />
    </form>
  )
}


export default SearchComponent;