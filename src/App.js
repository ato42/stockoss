import React, { useState } from 'react';

import './App.css';

import Stockoss from './lib/StockossTest';

import Sidebar from './Sidebar';
import Search from './Search';
import History from './History';
import Map from './Map';

console.log('empty', Stockoss());
console.log('1', Stockoss('a'));
console.log('5', Stockoss('abcde'));
console.log('6', Stockoss('abcdef'));
console.log('number', Stockoss(42));


function App() {
  const [searches, setSearches] = useState([]);
  const [error, setError] = useState('');
  const [position, setPosition] = useState('');

  const resetError = () => setError('');

  const showOnMap = (position) => setPosition(position);

  const addToHistory = (item) => setSearches([item, ...searches]);

  const handleSearch = (search) => {
    const res = Stockoss(search);
    if (res.error_message) {
      setError(res.error_message);
    } else {
      addToHistory({
        search,
        time: Date.now()
      });
      showOnMap(res.position);
    }
  }

  const handleHistoryEntryClick = (search) => {
    const res = Stockoss(search);
    resetError();
    if (res.error_message) {
      setError(res.error_message);
    } else {
      showOnMap(res.position);
    }
  }

  return (
    <div className="App">
      <Sidebar>
        <Search
          onSearch={handleSearch}
          resetError={resetError}
          error={error}
        />
        <History
          history={searches}
          onHistoryEntryClick={handleHistoryEntryClick}
        />
      </Sidebar>
      <Map position={position} />
    </div>
  );
}

export default App;
